# Design of Password Storage

Here describes design of Password Storage.

* [1. Requirements.](requirements.md)
* [2. Abstract user registration.](abstract_user_registration.md)
* [3. Abstract save password.](abstract_save_password.md)
* [4. Abstract get password.](abstract_get_password.md)
* [5. Abstract remove password.](abstract_remove_password.md)
