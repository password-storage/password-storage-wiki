# Requirements

## 1. Core

* Password should be stored in encrypted mode using modern cipher
* Core divides into three parts Client, Proxy-Server, Server
* Client:
  * should connect to Proxy-Server by socket
  * should use JSON-RPC for this connection
  * should store all user data in network request should be encrypted
* Proxy server:
  * should listen to requests from Client
  * should connect to Server by socket
  * should use JSON-RPC for this connections
  * should authenticate users
  * should generate API token
* Server:
  * should listen to requests from Proxy-Server
  * should use JSON-RPC for this connections
  * should store encrypted passwords
  * should authenticate users
  * should store passwords by key(label)
  * should encrypt stored passwords with user password
  * should generate API token
* Code should be scalabel
* Also here exists not "logic" module name Auth-Server, it should auth users and generates JWT