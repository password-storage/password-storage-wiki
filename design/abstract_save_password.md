# Abstract save password

## 1. Client send request to Proxy-Server.

Request consists of:
* API Token
* username
* bcrypt hash of password with salt
* salt

## 2. Proxy-Server auth user.

Returns JWT for user and begins Diffie–Hellman for new AES key.

## 3. Client send request to Proxy-Server with AES encrypted password to save.

Request consists of:
* API Token
* JWT
* encrypted password
* password label

## 3. Proxy-Server calls Server such as Client -> Proxy-Server.

## 4. Server stores password.

