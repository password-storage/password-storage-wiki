# Abstract user registration

## 1. Client send request to Proxy-Server.

Request consists of:
* API Token
* username
* bcrypt hash of password with salt
* salt

## 2. Proxy-Server register user.

Here exists Auth-Server. Proxy-Server calls it and register user here

# Abstract user auth

## 1. Client send request to Proxy-Server.

Request consists of:
* API Token
* username
* bcrypt hash of password with salt
* salt

## 2. Proxy-Server auth user.

Proxy-Server calls Auth-Server and checks user existing.
Auth-Server generates JWT for user, that lives abount 5 mins.