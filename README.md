# Documentation of Password Storage

Password storage is a system for secured password storing on remote server.

## Table of content

### [1. Design.](design/README.md)
#### [1.2. Requirements.](design/requirements.md)
#### [1.3. Abstract user registration.](design/abstract_user_registration.md)
#### [1.4. Abstract save password.](design/abstract_save_password.md)
#### [1.5. Abstract get password.](design/abstract_get_password.md)
#### [1.6. Abstract remove password.](design/abstract_remove_password.md)
